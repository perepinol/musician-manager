from datetime import date, timedelta
from django import forms
from django.core import validators
from django.core.exceptions import ValidationError
from django.db.models import query
from django.forms.fields import DurationField, MultiValueField
from django.forms.widgets import TextInput

from . import models, util

CURRENT_YEAR = date.today().year
YEAR_CHOICES = list(range(CURRENT_YEAR - 10, CURRENT_YEAR + 10))

class ProjectForm(forms.ModelForm):
  year = forms.IntegerField(widget=forms.Select(choices=list(map(lambda y: (y, util.get_season(y)), YEAR_CHOICES))), initial=CURRENT_YEAR)

  class Meta:
    model = models.Project
    fields = ['name', 'conductor', 'year']
  

class ParticipationForm(forms.ModelForm):
  class Meta:
    model = models.Project
    fields = ['participants']


class HistoricalForm(forms.ModelForm):
  since = forms.DateField(initial=date.today)
  amount = forms.FloatField(min_value=0, help_text='')

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.fields['amount'].help_text = self.Meta.model.unit
  
  def is_valid(self):
    if super().is_valid() and self.Meta.model.objects.filter(since=self.cleaned_data['since']).count() == 0:
      return True
    self.add_error('since', 'Cannot have two values on the same date. Edit or delete the existing value')
    return False

  class Meta:
    model = models.Historical
    fields = ['amount', 'since']


class MileagePriceForm(HistoricalForm):
  class Meta(HistoricalForm.Meta):
    model = models.MileagePrice


class RehearsalPriceForm(HistoricalForm):
  class Meta(HistoricalForm.Meta):
    model = models.RehearsalPrice


class ConcertPriceForm(HistoricalForm):
  class Meta(HistoricalForm.Meta):
    model = models.ConcertPrice


class ResidenceForm(forms.ModelForm):
  since = forms.DateField(initial=date.today)
  
  def is_valid(self):
    if super().is_valid() and models.Residence.objects.filter(since=self.cleaned_data['since']).count() == 0:
      return True
    self.add_error('since', 'Cannot have two values on the same date. Edit or delete the existing value')
    return False
  
  class Meta:
    model = models.Residence
    fields = ['since', 'location']


class LocationWidget(forms.MultiWidget):
  def __init__(self, **kwargs):
    super().__init__(**kwargs)
  
  def decompress(self, value):
    return [value.since, value.location.name] if value else [date.today, None]


class ResidenceField(forms.MultiValueField):
  def __init__(self, **kwargs):
    fields = (forms.DateField(initial=date.today), forms.ModelChoiceField(queryset=models.Location.objects.all()))
    super().__init__(
      fields=fields,
      widget=LocationWidget(widgets=[field.widget for field in fields]),
      **kwargs
    )
  
  def compress(self, data_list):
    return { 'since': data_list[0], 'location': models.Location.objects.filter(name=data_list[1]).get() }


class MusicianForm(forms.ModelForm):
  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self.fields['last_residence'] = ResidenceField(initial=self.instance.residence_set.latest() if self.instance.residence_set.count() > 0 else None)
    del self.fields['location']
  
  def clean(self):
    cleaned_data = super().clean()
    if models.Residence.objects.filter(since=cleaned_data['last_residence']['since'], musician=self.instance).count() > 0:
      raise ValidationError('Cannot create two residences at the same date')
  
  def save(self, commit = ...):
    location = self.cleaned_data['last_residence']['location']
    musician = super().save(commit=commit)
    if commit:
      models.Residence(since=self.cleaned_data['last_residence']['since'], location=location, musician=self.instance).save()
    return musician

  class Meta:
    model = models.Musician
    fields = ['first_name', 'last_name_1', 'last_name_2', 'instrument', 'location', 'other_info']


class RehearsalForm(forms.ModelForm):

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    assistances = list(self.instance.participants.through.objects.all())
    project_musicians = self.instance.project.participants.all()
    for musician in project_musicians:
      if len(list(filter(lambda x: x.musician.id == musician.id, assistances))) == 0:
        assistances += [models.RehearsalAssistance(musician=musician, rehearsal=self.instance, duration=timedelta())]
    
    del self.fields['participants']
    for assistance in assistances:
      self.fields['participant_%d' % assistance.musician.id] = DurationField(initial=assistance.duration)

  
  def clean(self):
    cleaned_data = super().clean()
    for id, value in [item for item in self.cleaned_data.items() if item[0].startswith('participant_')]:
      if value > self.cleaned_data['duration']:
        raise ValidationError('Participation cannot exceed rehearsal duration')

    result = {
      'participants': []
    }
    for id, value in cleaned_data.items():
      if id.startswith('participant_'):
        if value:
          result['participants'] += [models.RehearsalAssistance(musician_id=id[id.index('_') + 1:], rehearsal_id=self.instance.id, duration=value)]
      else:
        result[id] = value
    return result
  
  def save(self, commit = ...):
    participants = self.cleaned_data['participants']
    self.cleaned_data['participants'] = []
    rehearsal = super().save(commit=commit)
    if commit:
      [assistance.save() for assistance in participants]
    return rehearsal
    

  class Meta:
    model = models.Rehearsal
    fields = ['date', 'duration', 'participants']
