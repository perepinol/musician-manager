from enum import Enum
from django.core.exceptions import ObjectDoesNotExist
from django.db import models

from . import util

# This order is used for sorting
class Instruments(Enum):
    SOPRANO = 0
    CONTRALTO = 1
    TENOR = 2
    BASS = 3

    @classmethod
    def choices(cls):
        return tuple((i.value, i.name) for i in cls)


def get_latest_or_none(model_name):
    try:
        return model_name.objects.latest()
    except ObjectDoesNotExist:
        return None


class Historical(models.Model):
    since = models.DateField()
    amount = models.FloatField()
    name = None
    unit = ''

    def __str__(self):
        return '%s - %s %s' % (str(self.since), str(self.amount), self.unit)

    class Meta:
        abstract = True
        get_latest_by = 'since'


class Location(models.Model):
    name = models.CharField(max_length=50, primary_key=True)
    distance_to_home = models.IntegerField()

    def __str__(self) -> str:
        return self.name


class Musician(models.Model):
    first_name = models.CharField(max_length=30)
    last_name_1 = models.CharField(max_length=30, null=True, blank=True)
    last_name_2 = models.CharField(max_length=30, null=True, blank=True)
    instrument = models.PositiveIntegerField(choices=Instruments.choices())
    location = models.ManyToManyField(Location, through='Residence')
    other_info = models.TextField()

    def get_name(self):
        return '%s %s' % (self.first_name, self.last_name_1) + (' %s' % self.last_name_2 if self.last_name_2 is not None else '')

    def get_instrument(self):
        return Instruments(self.instrument).name

    def __str__(self):
        return '%s (%s)' % (self.get_name(), Instruments(self.instrument).name)


class Residence(models.Model):
    since = models.DateField()
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    musician = models.ForeignKey(Musician, on_delete=models.CASCADE)

    def __str__(self):
        return '%s: %s since %s' % (self.musician.get_name(), self.location.name, self.since)
    
    class Meta:
        get_latest_by = 'since'


class Project(models.Model):
    name = models.CharField(max_length=50)
    year = models.PositiveIntegerField()
    conductor = models.CharField(max_length=90)
    participants = models.ManyToManyField(Musician)

    def get_season(self):
        return util.get_season(self.year)

    def __str__(self):
        return '%s - %s' % (str(self.name), self.get_season())


class Concert(models.Model):
    date = models.DateField()
    location = models.CharField(max_length=50)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    participants = models.ManyToManyField(Musician, blank=True)

    def __str__(self):
        return '%s: %s at %s' % (str(self.date), self.project.name, self.location)


class Rehearsal(models.Model):
    date = models.DateField()
    duration = models.DurationField()
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    participants = models.ManyToManyField(Musician, through='RehearsalAssistance', blank=True)

    def __str__(self):
        return '%s: %s' % (str(self.date), self.project.name)


class RehearsalAssistance(models.Model):
    musician = models.ForeignKey(Musician, on_delete=models.CASCADE)
    rehearsal = models.ForeignKey(Rehearsal, on_delete=models.CASCADE)
    duration = models.DurationField()

    def __str__(self):
        return '%s at (%s): %s minutes' % (self.musician.get_name(), self.rehearsal, str(self.duration))


class MileagePrice(Historical):
    name = 'Mileage price'
    unit = '€/km'


class RehearsalPrice(Historical):
    name = 'Rehearsal price'
    unit = '€/h'


class ConcertPrice(Historical):
    name = 'Concert price'
    unit = '€/h'
