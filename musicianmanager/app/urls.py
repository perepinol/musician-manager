from django.urls import path
from . import views

urlpatterns = [
    path('rehearsal/<int:pk>/delete', views.RehearsalDeletionView.as_view(), name='delete-rehearsal'),
    path('rehearsal/<int:pk>', views.RehearsalEditionView.as_view(), name='rehearsal-assistance'),

    path('concert/<int:pk>/delete', views.ConcertDeletionView.as_view(), name='delete-concert'),
    path('concert/<int:pk>', views.ConcertEditionView.as_view(), name='concert-assistance'),

    path('project/<int:pk>/rehearsal', views.RehearsalCreationView.as_view(), name='new-rehearsal'),
    path('project/<int:pk>/concert', views.ConcertCreationView.as_view(), name='new-concert'),
    path('project/<int:pk>/participants', views.ParticipantEditionView.as_view(), name='participant-edition'),
    path('project/<int:pk>/delete', views.ProjectDeletionView.as_view(), name='delete-project'),
    path('project/<int:pk>', views.ProjectDetailView.as_view(), name='project-detail'),
    path('project', views.ProjectCreationView.as_view(), name='project-creation'),

    path('residence/<int:pk>/delete', views.ResidenceDeletionView.as_view(), name='delete-residence'),
    path('residence/<int:pk>', views.ResidenceEditionView.as_view(), name='residence-edition'),

    path('musician/<int:pk>/residence', views.ResidenceCreationView.as_view(), name='residence'),
    path('musician/<int:pk>', views.MusicianDetailView.as_view(), name='musician'),
    path('musician', views.MusicianCreationView.as_view(), name='musician_creation'),

    path('historical/mileage/<int:pk>/delete', views.MileagePriceHistoryDeletionView.as_view(), name='delete-mileage-history'),
    path('historical/mileage/<int:pk>', views.MileagePriceHistoryEditionView.as_view(), name='mileage-history-edition'),
    path('historical/mileage', views.MileagePriceHistoryCreationView.as_view(), name='mileage-history'),

    path('historical/rehearsal/<int:pk>/delete', views.RehearsalPriceHistoryDeletionView.as_view(), name='delete-rehearsal-history'),
    path('historical/rehearsal/<int:pk>', views.RehearsalPriceHistoryEditionView.as_view(), name='rehearsal-history-edition'),
    path('historical/rehearsal', views.RehearsalPriceHistoryCreationView.as_view(), name='rehearsal-history'),

    path('historical/concert/<int:pk>/delete', views.ConcertPriceHistoryDeletionView.as_view(), name='delete-concert-history'),
    path('historical/concert/<int:pk>', views.ConcertPriceHistoryEditionView.as_view(), name='concert-history-edition'),
    path('historical/concert', views.ConcertPriceHistoryCreationView.as_view(), name='concert-history'),

    path('location', views.LocationListView.as_view(), name='locations'),
    path('', views.Overview.as_view(), name='overview')
]
