from django.http.response import HttpResponseBadRequest
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views import generic
from . import forms

from .models import ConcertPrice, Location, MileagePrice, Project, Musician, Rehearsal, Concert, RehearsalPrice, Residence, get_latest_or_none

# Create your views here.
class DeletionView(generic.DeleteView):
    template_name = 'app/delete_confirmation.html'

    def get_back_url(self):
        raise NotImplementedError('Subclasses should override this method')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['back_url'] = self.get_back_url()
        return context


class Overview(generic.TemplateView):
    template_name = 'app/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['projects'] = Project.objects.all()
        context['musicians'] = Musician.objects.all()
        context['historicals'] = {
            'mileage_price': get_latest_or_none(MileagePrice),
            'rehearsal_price': get_latest_or_none(RehearsalPrice),
            'concert_price': get_latest_or_none(ConcertPrice)
        }
        return context


class HistoryCreationView(generic.CreateView):
    template_name = 'app/historical_list.html'
    edit_url_name = ''
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['historical_name'] = self.model.name
        context['unit'] = self.model.unit
        context['history'] = self.model.objects.all()
        context['edit_url_name'] = self.edit_url_name
        return context

    def get_success_url(self):
        return ''


class HistoryEditionView(generic.UpdateView):
    template_name = 'app/historical_form.html'
    fields = ['since', 'amount']
    delete_url_name = ''
    back_url_name = ''

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['delete_url_name'] = self.delete_url_name
        context['back_url_name'] = self.back_url_name
        return context
    
    def get_success_url(self) -> str:
        return reverse(self.back_url_name)


class HistoryDeletionView(DeletionView):
    back_url_name = ''

    def get_back_url(self):
        return reverse(self.back_url_name, kwargs={'pk': self.get_object().id})


class MileagePriceHistoryCreationView(HistoryCreationView):
    model = MileagePrice
    form_class = forms.MileagePriceForm
    edit_url_name = 'mileage-history-edition'


class MileagePriceHistoryEditionView(HistoryEditionView):
    model = MileagePrice
    back_url_name = 'mileage-history'
    delete_url_name = 'delete-mileage-history'


class MileagePriceHistoryDeletionView(HistoryDeletionView):
    model = MileagePrice
    back_url_name = 'mileage-history-edition'

    def get_success_url(self):
        return reverse('mileage-history')


class RehearsalPriceHistoryCreationView(HistoryCreationView):
    model = RehearsalPrice
    form_class = forms.RehearsalPriceForm
    edit_url_name = 'rehearsal-history-edition'


class RehearsalPriceHistoryEditionView(HistoryEditionView):
    model = RehearsalPrice
    back_url_name = 'rehearsal-history'
    delete_url_name = 'delete-rehearsal-history'


class RehearsalPriceHistoryDeletionView(HistoryDeletionView):
    model = RehearsalPrice
    back_url_name = 'rehearsal-history-edition'

    def get_success_url(self):
        return reverse('rehearsal-history')


class ConcertPriceHistoryCreationView(HistoryCreationView):
    model = ConcertPrice
    form_class = forms.ConcertPriceForm
    edit_url_name = 'concert-history-edition'


class ConcertPriceHistoryEditionView(HistoryEditionView):
    model = ConcertPrice
    back_url_name = 'concert-history'
    delete_url_name = 'delete-concert-history'


class ConcertPriceHistoryDeletionView(HistoryDeletionView):
    model = ConcertPrice
    back_url_name = 'concert-history-edition'

    def get_success_url(self):
        return reverse('concert-history')


class ProjectCreationView(generic.CreateView):
    model= Project
    template_name = 'app/project_creation.html'
    form_class = forms.ProjectForm

    def get_success_url(self):
        return reverse('project_detail', kwargs={'pk': self.object.pk})


class ProjectDetailView(generic.UpdateView):
    model = Project
    template_name = 'app/project_detail.html'
    form_class = forms.ProjectForm

    def get_success_url(self):
        return ''


class ProjectDeletionView(DeletionView):
    model = Project

    def post(self, request, *args, **kwargs):
        if self.get_object().rehearsal_set.count() > 0 or self.get_object().concert_set.count() > 0:
            return HttpResponseBadRequest('Cannot delete project if it contains concerts or rehearsals')
        return super().post(request, *args, **kwargs)
    
    def get_success_url(self):
        return reverse('overview')
    
    def get_back_url(self):
        return reverse('project-detail', kwargs={'pk': self.get_object().id})


class RehearsalCreationView(generic.CreateView):
    model = Rehearsal
    template_name = 'app/project_subitem_form.html'
    fields = ['date', 'duration']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['project'] = get_object_or_404(Project, pk=self.kwargs['pk'])
        context['subitem_name'] = 'rehearsal'
        return context

    def form_valid(self, form):
        form.instance.project = get_object_or_404(Project, pk=self.kwargs['pk'])
        return super(RehearsalCreationView, self).form_valid(form)

    def get_success_url(self):
        return reverse('project-detail', kwargs={'pk': self.kwargs['pk']})


class RehearsalEditionView(generic.UpdateView):
    model = Rehearsal
    template_name = 'app/rehearsal_edition.html'
    form_class = forms.RehearsalForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['musicians'] = self.get_object().project.participants.all()
        return context

    def get_success_url(self):
        return reverse('project-detail', kwargs={'pk': self.kwargs['pk']})


class RehearsalDeletionView(DeletionView):
    model = Rehearsal

    def get_success_url(self):
        return reverse('project-detail', kwargs={'pk': self.get_object().project.id})
    
    def get_back_url(self):
        return reverse('rehearsal-assistance', kwargs={'pk': self.get_object().id})


class ConcertCreationView(generic.CreateView):
    model = Concert
    template_name = 'app/project_subitem_form.html'
    fields = ['date', 'location']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['project'] = get_object_or_404(Project, pk=self.kwargs['pk'])
        context['subitem_name'] = 'concert'
        return context

    def form_valid(self, form):
        form.instance.project = get_object_or_404(Project, pk=self.kwargs['pk'])
        return super(ConcertCreationView, self).form_valid(form)

    def get_success_url(self):
        return reverse('project-detail', kwargs={'pk': self.kwargs['pk']})


class ConcertEditionView(generic.UpdateView):
    model = Concert
    template_name = 'app/concert_edition.html'
    fields = ['date', 'location', 'participants']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['musicians'] = self.get_object().project.participants.all()
        return context

    def get_success_url(self):
        return reverse('project-detail', kwargs={'pk': self.kwargs['pk']})


class ConcertDeletionView(DeletionView):
    model = Concert
    
    def get_success_url(self):
        return reverse('project-detail', kwargs={'pk': self.get_object().project.id})
    
    def get_back_url(self):
        return reverse('concert-assistance', kwargs={'pk': self.get_object().id})


class ParticipantEditionView(generic.UpdateView):
    model = Project
    template_name = 'app/project_participant_edition.html'
    fields = ['participants']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['musicians'] = Musician.objects.all()
        return context

    def get_success_url(self):
        return reverse('project-detail', kwargs={'pk': self.kwargs['pk']})


class MusicianCreationView(generic.CreateView):
    model = Musician
    template_name = 'app/musician_detail.html'
    form_class = forms.MusicianForm

    def get_success_url(self) -> str:
        return reverse('musician', kwargs={'pk': self.object.id})


class MusicianDetailView(generic.UpdateView):
    model = Musician
    template_name = 'app/musician_detail.html'
    fields = ['first_name', 'last_name_1', 'last_name_2', 'instrument', 'other_info']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_success_url(self) -> str:
        return ''


class ResidenceCreationView(generic.CreateView):
    model = Residence
    form_class = forms.ResidenceForm
    template_name = 'app/residence_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['musician'] = get_object_or_404(Musician, id=self.kwargs['pk'])
        context['history'] = context['musician'].residence_set.all()
        return context
    
    def form_valid(self, form):
        form.instance.musician = get_object_or_404(Musician, id=self.kwargs['pk'])
        return super().form_valid(form)

    def get_success_url(self):
        return ''


class ResidenceEditionView(generic.UpdateView):
    model = Residence
    fields = ['since', 'location']
    template_name = 'app/residence_form.html'

    def get_success_url(self):
        return reverse('residence', kwargs={'pk': self.get_object().musician.id})


class ResidenceDeletionView(DeletionView):
    model = Residence

    def get_success_url(self):
        return reverse('residence', kwargs={'pk': self.get_object().musician.id})

    def get_back_url(self):
        return reverse('residence-edition', kwargs={'pk': self.get_object().id})


class LocationListView(generic.CreateView):
    model = Location
    template_name = 'app/location_list.html'
    fields = ['name', 'distance_to_home']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['locations'] = Location.objects.all()
        return context

    def get_success_url(self) -> str:
        return ''
