def get_season(year):
  return '%d/%02d' % (year - 1, year - year // 100 * 100)