# Generated by Django 3.2.9 on 2021-12-04 10:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='concert',
            name='participants',
            field=models.ManyToManyField(to='app.Musician'),
        ),
        migrations.AlterField(
            model_name='musician',
            name='instrument',
            field=models.PositiveIntegerField(choices=[(0, 'SOPRANO'), (1, 'CONTRALTO'), (2, 'TENOR'), (3, 'BASS')]),
        ),
    ]
