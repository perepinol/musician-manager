# Generated by Django 3.2.9 on 2021-12-04 22:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20211204_1014'),
    ]

    operations = [
        migrations.AlterField(
            model_name='concert',
            name='participants',
            field=models.ManyToManyField(blank=True, to='app.Musician'),
        ),
        migrations.AlterField(
            model_name='musician',
            name='last_name_1',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='rehearsal',
            name='participants',
            field=models.ManyToManyField(blank=True, through='app.RehearsalAssistance', to='app.Musician'),
        ),
    ]
