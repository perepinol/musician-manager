from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Musician)
admin.site.register(Location)
admin.site.register(Residence)
admin.site.register(Project)
admin.site.register(Concert)
admin.site.register(Rehearsal)
admin.site.register(RehearsalAssistance)
admin.site.register(MileagePrice)
admin.site.register(ConcertPrice)
admin.site.register(RehearsalPrice)
